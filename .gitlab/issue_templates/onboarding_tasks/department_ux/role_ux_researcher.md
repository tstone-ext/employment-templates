#### UX Researchers

<details>
<summary>New Team Member</summary>

1. [ ] Join the [UX channel](https://gitlab.slack.com/archives/C03MSG8B7) on Slack.
1. [ ] Join the [UX research channel](https://gitlab.slack.com/archives/CMEERUCE4) on Slack.
1. [ ] Join the [UX research team lounge channel](https://gitlab.slack.com/archives/C016GDHMG5Q) on Slack
1. [ ] Familiarize yourself with the [UX Research pages](https://about.gitlab.com/handbook/engineering/ux/ux-research/) in the handbook.
1. [ ] Review [product operations in the product handbook](https://about.gitlab.com/handbook/product/product-operations/) and set up a coffee chat with with Product Operations `@fseifoddini`
1. [ ] Ask questions in the #product-operations or #product Slack channels to get clarification on things you are uncertain about
1. [ ] *Optional* Attend [PM Meeting bi-weekly and Product Operations bi-weekly meetings](https://about.gitlab.com/handbook/product/product-processes/#team-meetings) (they alternate and are on the company calendar)
</details>


<details>
<summary>Manager</summary>

1. [ ] Add new team member to UX Department weekly call.
1. [ ] Add new team member to UX Research weekly call.
1. [ ] Give new team member edit rights ('Make changes to events') for the UX team Google calendar
1. [ ] Add new team member to UX Hangout calls.
1. [ ] Give new team member `Developer` access to the [UX Retrospective](https://gitlab.com/gl-retrospectives/ux-retrospectives) project on GitLab.com
1. [ ] Add new team member to the [UX Research Team issue board](https://gitlab.com/groups/gitlab-org/-/boards/1540253).
1. [ ] Announce new team member's arrival in UX Department weekly call.
1. [ ] Announce new team member's arrival in the Engineering week in review.

</details>
