#### Infrastructure Department

<details>
<summary>New Team Member</summary>

##### Technical Git Information

1. [ ] Order your [Yubikey](https://www.yubico.com/products/) which you will use for authentication in your SRE onboarding issue.
1. [ ] Become familiar with GitLab. Familiarize yourself with the dashboard, the projects, and the issue tracker. Become familiar with the `README.md`s for these projects:
   * [GitLab www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/README.md)
   * [GitLab](https://gitlab.com/gitlab-org/gitlab/blob/master/README.md)
   * [GitLab FOSS Edition](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/README.md).
**Note:** that the Gitlab FOSS (Free Open Source Software) Edition is a read-only mirror of GitLab with the proprietary software removed. [This issue](https://gitlab.com/gitlab-org/gitlab/issues/13304) covers the merging of the Enterprise and Community editions.
1. [ ] Refer to [GitLab Docs](https://docs.gitlab.com/ee/README.html) for information on features of GitLab.
1. [ ] Download and [get started with Git](https://docs.gitlab.com/ce/gitlab-basics/start-using-git.html).  It is important for all team members to understand how to work both on the web interface and locally. The team is happy to assist in teaching you git.
1. [ ] Consider subscribing to [status.gitlab.com](https://status.gitlab.com) to stay aware of active incidents.

1. [ ] Familiarize yourself with the [Infrastructure handbook](https://about.gitlab.com/handbook/engineering/infrastructure/) and [Engineering function handbook](https://about.gitlab.com/handbook/engineering).
1. [ ] Join the channels listed in the [keeping yourself informed engineering section](https://about.gitlab.com/handbook/engineering/#sts=Keeping%20yourself%20informed)
1. [ ] Review your team [handbook page] (https://about.gitlab.com/handbook/engineering/infrastructure/team/) and look for additional onboarding steps.
</details>

<details>
<summary>Manager</summary>

1. [ ] Create a new onboarding checklist based on your team onboarding template.
   * [SRE](https://gitlab.com/gitlab-com/gl-infra/reliability/-/blob/master/.gitlab/issue_templates/onboarding-sre-machine-setup.md).
   * [Delivery](https://gitlab.com/gitlab-org/release/tasks/-/blob/master/.gitlab/issue_templates/Onboarding-Delivery-team-member.md)
   * [Scalability](https://gitlab.com/gitlab-com/gl-infra/scalability/-/blob/master/.gitlab/issue_templates/Onboarding.md)
</details>
