#### Professional Services Technical Architect

<details>
<summary>New Team Member</summary>


## Section 1: Undertanding the Technology

_Week 2-4_

**SandBox Environments**

https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#individual-aws-account-or-gcp-project

1. [ ] [Add AWS cloud sandbox](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#accessing-your-aws-account)
2. [ ] [Add GCP access](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#accessing-your-gcp-project)
3. [ ] [Add GitLab Demo Cloud access](https://about.gitlab.com/handbook/customer-success/demo-systems/#access-shared-omnibus-instances)

See AWS and GCP above.
**Certifications**
1. [ ] Complete all [GitLab Certifications](https://about.gitlab.com/services/education/gitlab-technical-certification-self-paced/) on GitLab Learn.
    - [ ] [GitLab Associate](https://gitlab.edcast.com/pathways/ECL-59bc8feb-9a02-4cfd-890b-bfec21534688)
    - [ ] [GitLab CI/CD Specialist](https://gitlab.edcast.com/pathways/ECL-cfff5c72-89c2-462e-9076-444b5f466ad1)
    - [ ] [GitLab Security Specialist](https://gitlab.edcast.com/pathways/ECL-74bad781-33d5-4ce3-9c2c-d89b21e17d22)
    - [ ] [GitLab Project Manager Specialist](https://gitlab.edcast.com/pathways/ECL-76320cec-75d4-4dce-979f-eb8a88c301e6)

**Cross-team Collaboration**
1. [ ] Shadow/pair with a [Support team member](https://about.gitlab.com/team/) (appropriate to timezone) on 3-5 calls
2. [ ] Work with your Manager to Update our [Skills Matrix](https://docs.google.com/spreadsheets/d/1SaojIbH875KuPmls_oChiRACF2HOFi0I-p_vJ1B9t40/edit#gid=0) so that Operations find Engagements you will be successful with in the future.

**GET Workshop/Implementation Certification**
1. [ ] Work through the [Implementation Engineer Specialist](https://gitlab.edcast.com/pathways/ECL-4f7c9aef-732e-4cde-ba09-e96ecaca464e). In this certificationk you will complete the [GET Deployment Workshop](https://gitlab.com/gitlab-org/professional-services-automation/tools/implementation/get-deployment-workshop). :exclamation: The instance created here will be used in the next sections. DO NOT DELETE.

**Advanced Technical Topics**
1. [ ] Backup omnibus using our [Backup rake task](http://docs.gitlab.com/ce/raketasks/backup_restore.html#create-a-backup-of-the-gitlab-system)
1. [ ] Install GitLab via [Docker](https://docs.gitlab.com/ce/install/docker.html)
1. [ ] Restore backup to your Docker VM using our [Restore rake task](http://docs.gitlab.com/ce/raketasks/backup_restore.html#restore-a-previously-created-backup)
1. [ ] Starting a [Rails Console](https://docs.gitlab.com/ee/administration/operations/rails_console.html)
1. [ ] Learn about the [GitLab Rake](https://docs.gitlab.com/ee/raketasks/) command
1. [ ] Learn about [GitLab Omnibus Maintenance](https://docs.gitlab.com/omnibus/maintenance/) commands
1. [ ] Browse/get familiar with the [GitLab Support Templates](https://gitlab.com/gitlab-com/support/support-training/-/tree/master/.gitlab/issue_templates)
1. [ ] Set up [GitLab CI](https://docs.gitlab.com/ee/ci/quick_start/)

**Custom Integrations**
**Goal:** Understand how developers and customers can produce custom integrations to GitLab. If you want to integrate with GitLab there are three possible paths you can take:
1. [ ] [Utilizing webhooks](https://docs.gitlab.com/ce/user/project/integrations/webhooks.html) - If you want to reflect information from GitLab or initiate an action based on a specific activity that occurred on GitLab you can utilize the existing infrastructure of our webhooks. To read about setting up webhooks on GitLab visit this page.
1. [ ] [API integration](https://docs.gitlab.com/ee/api/README.html) - If you're seeking a richer integration with GitLab, the next best option is to utilize our ever-expanding API. The API contains pretty much anything you'll need, however, if there's an API call that is missing we'd be happy to explore it and develop it.
1. [ ] [Project Integrations](https://docs.gitlab.com/ee/user/project/integrations/) -  Integrations are like plugins, and give you the freedom to add functionality to GitLab.

**Migrations**
**Goal:** Understand how migrations from other Version Control Systems to GitLab using [Congregate](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate) work.
1. [ ] Work through the [Migration Engineer Specialist Certification](https://gitlab.edcast.com/pathways/ECL-b36d0c5d-b798-4ce3-8cd0-b8d80232c34d). In this course, you will complete the[Congregate Migration Workshop](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate-onboarding-workshop)

## Section 2: Understanding our business, services and processes

_Weeks 2-5_

- [ ] Make sure you are enrolled in [Sales Quick Start (SQS)](https://about.gitlab.com/handbook/sales/onboarding/). If you are not, reach out to your manager to ensure you're scheduled. 
- [ ] Watch the latest high level professional services enablement [recording](https://gitlab.edcast.com/insights/sqs-22-professional). And/or check out the [associated slides](https://docs.google.com/presentation/d/1SIg3xYyFVQIPEe36VpsCJqt3sDp5boma4YpICRQMrEA/edit#slide=id.g75e3e400c6_6_123).
- [ ] Review the high level [professional services methodology](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/processes/) to understand how the Project Manager workflows fit into the larger Sales process. 
    - [ ] Learn Agile Project Management with GitLab [Agile Project Management](https://www.youtube.com/watch?v=_dndOgHLvsM)
    - [ ] Understand how to navigate [SalesForceDotCom (SFDC) Fields & Reports](https://www.loom.com/share/111a577fbd8a4d6baeea1f2aee86dff2)
    - [ ] Review the different types of [professional services offerings and delivery kits available](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/framework/)
        - [ ] Learn about key details that tend to come up in [Migration Opportunities (25 mins)](https://www.loom.com/share/e02695d1092f476aaabdbd48e4c0a3f4?sharedAppSource=personal_library)
        - [ ] Understand the transformational services that we call the [Pipeline COE (8 mins)](https://www.loom.com/share/1c8e74a0728245a5b49274ba668e750e?sharedAppSource=personal_library)
        - [ ] Learn the fundamentals of [Distributied Systems Architecture (18 mins)](https://www.loom.com/share/6c6acebabce2474589eab0bea1879c2b?sharedAppSource=personal_library) to ensure you can speak the language required to sell/scope implementation services. 
            - [ ] Understand the basics of [Disaster Recovery (12 mins)](https://www.loom.com/share/535dddbe3eb247938fb2ba2e98eb625b?sharedAppSource=personal_library)
            - [ ] Learn about the [deployment Automation & Cloud Maturity (8 mins)](https://www.loom.com/share/9bd2745dbabb4384ac9a8aa369d79448?sharedAppSource=personal_library) 
        - [ ] Understand the differences between [GitLab SaaS vs Self Managed (7 mins)](https://www.loom.com/share/4772f2acf51d4d2a874ca2ce583fd94a?sharedAppSource=personal_library)
    - [ ] Review the different types of [education services](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/#current-offerings) and when to position them.  
    - [ ] Learn how we encourage the Sales Account teams to [sell professional services](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/selling/). Make sure to take note of the custom SOW process vs the standard SKU process. 
- [ ] Review the README files in the [Professional Services Delivery Kits](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits) to understand in more detail how we deliver services. 
    - [ ] Its important to note the `customer` foloder in the [Migration Template](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/migration-template) Project. There is lots of information about setting customer's expectations
    - [ ] The [migration customer documents](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/migration-template/-/tree/master/customer) are a great place to learn more about migration features and checklists that are discussed with the customer during pre-sales.
    - [ ] [Implementations](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/implementation-template)
    - [ ] [Readiness Assessments](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/readiness-assessment) (Health Checks)
    - [ ] Rapid Results ([SaaS](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/rapid-results-com)) ([self-managed](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/rapid-results-self-managed))
- [ ] Spend some the time in the [Practice Management folder](https://drive.google.com/drive/folders/1r0vqvcZObuLWYfa41mYHR3XJCmCKtiiO).  
- [ ] Request access to a ZenDesk light agent account following the process [here](https://about.gitlab.com/handbook/support/internal-support/#requesting-a-zendesk-light-agent-account)

## Section 3 - Schedule Coffee Chats

_Weeks 2-5_
- [ ] Schedule/Attend Coffee Chats:
_See the [roster](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/#team-members-and-roles) to find teammates names._
    - [ ] Have a coffee chat with peer Techincal Architect
    - [ ] Have a coffee chat with a Senior Professional Services Engineer, Consulting Services
    - [ ] Have a coffee chat with a Training, Education Services
    - [ ] Have a coffee chat with Project Coordinator to discuss back office operations and pre-sales to post-sales handoff
    - [ ] have a coffee chat with the Consulting Delivery Leadership team to discuss recent customer engagements

## Section 4 - Shadowing

_weeks 3-6_

- [ ] Shadow one or more Engagement Managers on local opportunities. Make a note of each in a comment on this issue. In each comment, make a note of the notes doc, scoping issue, and meetings scheduled to talk with that customer. 
    - [ ] Opportunity 1
    - [ ] Opportunity 2 
    - [ ] Opportunity 3 

- [ ] Shadow one or more Professional Services Engineers on an engagement. 
    - [ ] Customer 1
    - [ ] Customer 2 
    - [ ] Customer 3 


</details>

