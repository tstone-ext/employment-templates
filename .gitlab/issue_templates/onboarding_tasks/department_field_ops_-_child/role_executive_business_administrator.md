#### Executive Business Administrator Tasks

<details>
<summary>New Team Member</summary>

1. [ ] Review the [Executive Business Administrators Handbook Page](https://about.gitlab.com/handbook/eba/)
1. [ ] Review the [Company Expense Handbook Page](https://about.gitlab.com/handbook/finance/expenses/)
1. [ ] Get [Scheduling Privileges for Zoom](https://support.zoom.us/hc/en-us/articles/201362803-Scheduling-privilege) from your Executives
1. [ ] Get Copilot Access to Expensify from your Executives - instructions [here](https://community.expensify.com/discussion/4783/how-to-add-or-remove-a-copilot)
1. [ ] Get [Delegate Access to TripActions](https://community.tripactions.com/s/article/delegate-self-serve#:~:text=Designating%20Delegate%20Access%20to%20Another%20Traveler&text=the%20below%20steps%3A-,Log%20in%20and%20navigate%20to%20your%20Profile%20from%20the%20TripActions,to%20give%20delegate%20permissions%20to.) from your Executives
1. [ ] Get Calendar Access from your Executives -instructions [here](https://support.google.com/calendar/answer/37082?hl=en)
1. [ ] Install the [Chrome Zoom Plugin](https://chrome.google.com/webstore/detail/zoom-scheduler/kgjfgplpablkjnlkjmjdecgdpfankdle?hl=en)
1. [ ] Bookmark the [EBA Drive](https://drive.google.com/drive/u/0/folders/0AOGOTUVPubCmUk9PVA)

</details>
