#### Marketing Digital Experience

<details>
<summary>New Team Member</summary>

1. [ ] Join the following slack channels:
    1. [#digital-experience-team](https://gitlab.slack.com/archives/CN8AVSFEY)
    1. [#digital-experience-water-cooler](https://gitlab.slack.com/archives/G01JH8CA8F3)
    1. [#marketing](https://gitlab.slack.com/archives/C0AKZRSQ5)
    1. [#website](https://gitlab.slack.com/archives/C62ERFCFM)
1. [ ] Read the [Digital Experience Handbook Page](https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/)

</details>


<details>
<summary>Manager</summary>

1. [ ] Provide [Figma](https://www.figma.com/files/972612628770206748/team/815314577342156102/GitLab-Experience-Design?fuid=854435822034786785) license by adding to account.
1. [ ] Provide Maintainer access to the [Digital Experience project](https://gitlab.com/gitlab-com/marketing/digital-experience).
1. [ ] Provide access to Google Analytics, Google Tag Manager, Google Search Console by submitting and AR Request
    1. [ ] Share all relevant dashboards
1. [ ] Provide access to [LogRocket](https://app.logrocket.com/9jygng/gitlab/).

</details>
