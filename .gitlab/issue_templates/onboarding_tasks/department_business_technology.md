#### Business Technology 

<details>
<summary>Manager/Lis Vinueza @lisvinueza</summary>

1. [ ] Invite to meetings: 
    *  [ ]  All Roles: Monthly Finance, bi-weekly Business Technology meeting
    *  [ ]  Enterprise Applications Team: Invite to Weekly sync, Daily office hours
    *  [ ]  Team Member Enablement (IT) Team: Weekly meeting
1. [ ] Invite to channels:
    * [ ] #bt-confidential
    * [ ] #finance-confidential
1. [ ] Invite to groups
    * [ ] businesstechnology@ google group

</details>

<details>
<summary>New Team Member</summary>

1. [ ] Join Slack channels: #finance, #business-technology, #bt-team-lounge.  
    * [ ]  If you are a BSA: #bt-business-engagement. You can reach out in #bt-business-engagement to the other BSAs to ask which channels you should join based on your area of focus
    * [ ]  If you are part of the Data Team: #data, #data-lounge. Find other channels to join in the [Data Handbook](https://about.gitlab.com/handbook/business-technology/data-team/#slack).
    * [ ]  If you are part of the IT Team: #it_help. Ask your manager to invite you to any private channels.
    * [ ]  If you are part of the Procurement team: #procurement

##### Technical Git Information

1. [ ] Become familiar with GitLab. Familiarize yourself with the dashboard, the projects, and the issue tracker. Become familiar with the `README.md`s for these projects:
   * [GitLab www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/README.md)
   * [GitLab](https://gitlab.com/gitlab-org/gitlab/blob/master/README.md)
   * [GitLab FOSS Edition](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/README.md).
**Note:** that the Gitlab FOSS (Free Open Source Software) Edition is a read-only mirror of GitLab with the proprietary software removed. [This issue](https://gitlab.com/gitlab-org/gitlab/issues/13304) covers the merging of the Enterprise and Community editions.
1. [ ] Refer to [GitLab Docs](https://docs.gitlab.com/ee/README.html) for information on features of GitLab.
1. [ ] Download and [get started with Git](https://docs.gitlab.com/ce/gitlab-basics/start-using-git.html).  It is important for all team members to understand how to work both on the web interface and locally. The team is happy to assist in teaching you git.
1. [ ] Consider subscribing to [status.gitlab.com](https://status.gitlab.com) to stay aware of active incidents.

</details>
