## For Product Management Only
<details>
<summary>New Team Member</summary>

1. [ ] Familiarize yourself with the [product stages](https://about.gitlab.com/stages-devops-lifecycle/).
1. [ ] For the [section, stage and group]((https://about.gitlab.com/handbook/product/categories/)) you are assigned, join the `#s_$STAGENAME` and `#g_GROUPNAME` Slack channels.
1. [ ] Update PM [mapping of your product categories](https://about.gitlab.com/handbook/product/categories/).
    - This is done by making updates in [`stages.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/stages.yml) and [`_categories.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/categories.yml)
1. [ ] Schedule introductory calls with your Core Team as listed in the [Product Categories page](https://about.gitlab.com/handbook/product/categories/).
   1. [ ] Scheduled call with _____ EM
   1. [ ] Scheduled call with _____ Product Designer
   1. [ ] Scheduled call with _____ PMM
   1. [ ] Scheduled call with _____ UX Researcher
1. [ ] Schedule a "shadowing" session with your buddy. Set aside a hour or two to observe him/her doing their work and ask questions along the way

**Access**

1. [ ] If you need access to Staging, if not already granted, please follow the instructions on the [Staging section](https://about.gitlab.com/handbook/engineering/infrastructure/environments/#staging) in the engineering handbook.
1. [ ] Request a light agent ZenDesk account to view customer tickets: [support handbook](https://about.gitlab.com/handbook/support/internal-support/#light-agent-zendesk-accounts-available-for-all-gitlab-staff)
1. [ ] Request access for Chorus.ai using [Okta](https://gitlab.okta.com/app/UserHome). If you haven't received an invitation email yet, check the status of your access request issue or ask your manager for help.

 **PM Product and Company Onboarding**

1. [ ] Familiarize yourself with the company [Objective and Key Results](https://about.gitlab.com/company/okrs/), [how the Product team tracks them](https://about.gitlab.com/handbook/product/product-okrs/), and understand the implications for your stage.
1. [ ] Go through the [Product marketing demos](https://about.gitlab.com/handbook/marketing/product-marketing/demo/) to familiarize yourself with how we talk about our software to prospective customers.
1. [ ] Familiarize yourself with the Gitlab Unfiltered Youtube channel [Gitlab UN](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A)
1. [ ] Read the [stewardship](https://about.gitlab.com/company/stewardship/) page to learn how we think about our Stewardship of GitLab as an open source project.


</details>

<details>
<summary>Manager</summary>

1. [ ] Ask Legal team to add new team member in Slack #Legal by posting this message:
     - `@rerwin` Please note [`@mention team member`][start date: yyyy-mm-dd] needs to be added to the designated insider list. 
       cc Prod Ops `@fseifoddini`
1. [ ] Add the new team member to our asynchronous standup meetings in Slack with GeekBot by adding the teammate to the [#pm_standup slack channel](https://join.slack.com/share/enQtMjY5NzI0OTcxNjg4MC05N2EyOGI1ZmQ1YjBiYzc2OGQ0OTRjNjU3ODlmODhjMjhhN2UyMjk3OGZlZjU4MWYyNTcyNjllNGFiYWM2ZmM5).
1. [ ] Create and begin to action a [First 100 day plan](https://gitlab.com/gitlab-com/Product/-/issues/new?issuable_template=PM-onboarding-first-100-days) for the team member to help them be successful in their first 100 days at GitLab.

</details>
