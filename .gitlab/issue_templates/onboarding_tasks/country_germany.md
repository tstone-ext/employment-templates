### For Team Members in Germany

<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: Read through the GitLab GmbH benefits [page](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/gmbh-benefits-germany/).
1. [ ] Complete the [Health and Safety checklist](https://about.gitlab.com/handbook/legal/entity-specific-employment-policies/#germany)
1. [ ] Read through the GitLab GmbH entity [handbook page](https://about.gitlab.com/handbook/people-group/employment-solutions/entities/germany-gmbh/)
1. [ ] Join the slack channel #gitlab-germany
1. [ ] Make yourself familiar with [taxes related equity](https://about.gitlab.com/handbook/tax/stock-options/). There is a [blog post in German language](https://svij.org/blog/2022/09/16/versteuerung-von-us-aktienoptionen-und-paketen-in-deutschland/), which might be a good additional read for you. 
</details>

<details>
<summary>People Connect</summary>

1. [ ] People Connect: Following the guidelines documented in the [job aid](https://docs.google.com/document/d/1fKRau2JJcuHLxsHep4Z6zi6ylTSQxeQ-VsIhPKPITFc/edit) update the team members Probation Period details in Workday.
1. [ ] People Connect: Open the [GitLab GmbH Employee Payroll Form](https://docs.google.com/spreadsheets/d/1G9CcZpj-o3ZQ83SCy35__Rd8l9WjYDxa/edit#gid=1907185985) in Google Drive. Select `make a copy` and save in the new team members name. Share the form with the team members personal email address as well as `nonuspayroll @gitlab.com`.
1. [ ] People Connect: Email the new team member the [Germany email](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/main/email_templates/germany_email.md) and cc `nonuspayroll @gitlab.com`.

</details>

<details>
<summary>Legal</summary>

1. [ ] @sarahrogers Confirm that the team member has reviewed and acknowledged Home Checklist within their Workday Onboarding Tasks.

</details>
