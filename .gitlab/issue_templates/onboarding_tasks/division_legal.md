#### Legal Team Members

<details>
<summary>New Team Member</summary>

1. [ ] New Team Member: Request access to Legal On-Boarding Document [HERE](https://docs.google.com/spreadsheets/d/1VWnM5tcZgddRfB5xT2Ce_giLUYTPeKw_V9bEoRTGtJc/edit#gid=0)
1. [ ] New Team Member: Request access to Legal Shared Drive and applicable tools in [Legal Tech Stack](https://docs.google.com/spreadsheets/d/1nLtWVx6mebR7_y2Qv_CcScbVW-ryLVzvcFVgGk2yeRs/edit?usp=sharing): Contact @rnalen and/or @ktesh
1. [ ] New Team Member: Contract Managers request access to Contract Manager Playbook [HERE](https://docs.google.com/spreadsheets/d/1FhdSuzfzNQf2LqGHiIC3ifVSpWaPDW3oMZuoNzfyTD0/edit#gid=216334611)
1. [ ] New Team Member: Request access to #legal_team Slack Channel (Contact your Onboarding Buddy if there are issues)
1. [ ] New Team Member: Join #legal Slack Channel
</details>
