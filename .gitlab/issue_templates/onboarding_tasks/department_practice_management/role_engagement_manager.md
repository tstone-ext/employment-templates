#### Professional Services Engagement Manager

<details>
<summary>New Team Member</summary>

_Welcome to Engagement Manager Onboarding! This issue will help you get ramped up on being able to scope, position and sell services._

:warning: If you find something that can be improved, please submit a merge request to this [issue template](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/tree/main/.gitlab/issue_templates/onboarding_tasks/department_practice_management/role_engagement_manager.md) and send to the [#engagement-managers](https://gitlab.slack.com/archives/C021J8Z88AJ) slack channel for review. 

## Section 1: Meet the team and start to uunderstanding our business, services and processes

_Weeks 2-4_

- [ ] Make sure you are enrolled in [Sales Quick Start (SQS)](https://about.gitlab.com/handbook/sales/onboarding/). If you are not, reach out to your manager to ensure you're scheduled. 
- [ ] Watch the latest high level professional services enablement [recording](https://gitlab.edcast.com/insights/sqs-22-professional). And/or check out the [associated slides](https://docs.google.com/presentation/d/1SIg3xYyFVQIPEe36VpsCJqt3sDp5boma4YpICRQMrEA/edit#slide=id.g75e3e400c6_6_123).
- [ ] Review the high level [professional services methodology](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/processes/) to understand how the Engagement Manager workflows fit into the larger Sales process. 
- [ ] Enroll in the [EM Certification Learning Pathway](https://levelup.gitlab.com/learn/course/engagement-manager-accreditation). Note: we are moving to a new learning management platform in August 2022. If this link doesn't work, reach out to your manager to get the new link.  
    - [ ] Ask questions you have about any of the content in the EM Certification in the [#engagement-managers](https://gitlab.slack.com/archives/C021J8Z88AJ) slack channel
- [ ] Add a shortcut to the [Engagement Management folder](https://drive.google.com/drive/folders/1tEl6quPPujqGnz94isLFh8x9_DfKPpdG?usp=sharing) to your Google drive.
- [ ] Schedule/Attend Coffee Chats:
_See the [roster](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/#team-members-and-roles) to find teammates names._
    - [ ] Have a coffee chat with peer Engagement Managers - all 
    - [ ] Have a coffee chat with Practice Manager, Consulting Services
    - [ ] Have a coffee chat with Practice Manager, Education Services
    - [ ] Have a coffee chat with Project Coordinator to discuss back office operations and pre-sales to post-sales handoff
    - [ ] have a coffee chat with the Consulting Delivery Leadership team to discuss recent customer engagements

## Section 2 - Access

_Weeks 2-4_

- [ ] Make sure you've been added to the following slack channels. If you haven't make sure to fill out an [access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) to be added.
    - [ ] [#professional-services](https://gitlab.slack.com/archives/CFRLYG77X) 
        - General public channel for the company to communicate with PS.  Here PS answers questions, and collaborates with the account teams on PS opportunities.
    - [ ] [#ps-project-leadership](https://gitlab.slack.com/archives/GR4A7UJSF)
        - Private channel  used for discussing project coordination and project management with PS leadership.
    - [ ] [#ps-operations](https://gitlab.slack.com/archives/CNP5X078T)
        - Private channel is used to have conversations around Professional Services Operations, any billing, revenue and backend functions.
    - [ ] [#ps-engagement-mgrs](https://gitlab.slack.com/archives/C021J8Z88AJ)
        - Private channel used for PS engagement management collaboration and coordination.
    - [ ] [#congregate-internal](https://gitlab.slack.com/archives/GRYB20ZA5)
        - Private channel for collaborating on PS migrations using Congregate.
    - [ ] [#proliferate-internal](https://gitlab.slack.com/archives/G013HAR4EJZ)
        - Private channel used for collaborating on PS implementations using Proliferate.
    - [ ] [#ps_proservp](https://gitlab.slack.com/archives/GFY1QN4FJ)
        - Private channel used for internal PS team communications.
    - [ ] EM's slack group handle '@em' - https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/9346 
- [ ] Make sure you've been added to the `@em` group in Slack
- [ ] Review and Validate system accesses:
    - [ ] Salesforce.com
    - [ ] MavenLink
    - [ ] DocuSign
    - [ ] PSQuote
- [ ] If you've found that you're missing one or more access, please submit an MR to update the [engagement manager role based access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/blob/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_practice_management/role-engagement-manager.md)

## Section 3 - Research and understand DevOps and Software Development Domain
 - [ ] make a copy of the [research assignment document](https://docs.google.com/document/d/1TJlASMWdUXDvAryvJdMknJnQN3SCaBw9hxEnO56saV8/edit?usp=sharing)
 - [ ] post a sharing link to the copied document in this onboarding issue
 - [ ] follow the instructions in the doc to fill out the details about each section. Review (at least) weekly with your manager. Collaborate with the EM and wider PS team on what you've learned. 
 - [ ] Take this [beginner Udemy course on DevOps](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/). 
 - [ ] Share learnings with and ask questions of the EM team and your manager


## Section 4a - Review Customer Calls + Shadowing

_weeks 3-6_
- [ ] Review the recordings in the table below, documents and templates that will help understand the different types of conversations the EM has with customers. 
- [ ] Login to https://chorus.ai/.  This is a video recording application, and all scoping and estimate or SOW review calls are stored there.  
    - [ ] Once you have access search for "Julie Byrne" and you will find all calls that Julie attended/led.  We recommend watching one per day 
- [ ] Shadow a peer engagement manager on 5 opportunities from initial touchpoint with the account team all the way through to closed won/lost. Make a note of each in a comment on this issue. In each comment, make a note of the notes doc, scoping issue, and meetings scheduled to talk with that customer. 
    - [ ] Opportunity 1
    - [ ] Opportunity 2 
    - [ ] Opportunity 3 
    - [ ] Opportunity 4
    - [ ] Opportunity 5

| EM Step/Task | Customer Recording | Collateral | Template | 
| ------ | ------ | ------ | ------ |
| Large Customer Introductory | Recording TBD | Notes TBD | Template TBD |
| Large Customer A Discovery | [Large Customer A Estimate Discovery Call](https://chorus.ai/meeting/8B3988FFB49B4FBCBA01B4EB9D6D3465?tab=summary)  | [Large Customer A Discovery Notes Doc](https://docs.google.com/document/d/1RC5nU2oxo3UYiueW6G2nkhPb9aTxHayrQW1kHeVAn54/edit#) | Template TBD |
| Internal estimate building (Large Customer A) | [Estimate Recording]() | [Estimate Worksheet](https://docs.google.com/spreadsheets/d/14wyCtF4aPCo9nEh2hZ7olx_mRq4ziLmysMRoKcwtIdQ/edit#gid=498273375) | [Estimate Template](https://docs.google.com/spreadsheets/d/1YKMyflzsA-VPEVobB82zC8-n0hlC-uRBtiNB7Fm-kZg/edit#gid=498273375)
| Large Customer B Discovery | [Large Customer B Discovery Call](https://chorus.ai/meeting/781ADAC9197844F0A571FD05382366B9?ro_company_id=9765688)  |

## Section 4b - Review Selling Automation and SKU Process (Commercial EM ONLY)
- [ ] Understand how Account teams can quote PS SKUs directly on a license quote or on a separate PS Only Opportunity in SFDC. 
- [ ] Learn about the SKU creation process ([handbook](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/practice-mgmt/#how-to-create-a-new-service-sku), [example SKU](https://gitlab.com/gitlab-com/Finance-Division/finance/-/issues/4482))
- [ ] Schedule a chat with Bryan on this to ask questions as the SKU process. He can discuss the revenue, accounting and quoting tool constraints involved in the SKU creation process.  

## Section 5 - Product and Sales Training

_Weeks 4-8_

- [ ] Complete the ([GitLab with Git Basics course](https://levelup.gitlab.com/courses/gitlab-certified-git-associate-prep)). Make sure that you use the employee discount code in the link to enroll for free.
- [ ] Compelte the [GitLab for Project Managers Course](https://levelup.gitlab.com/courses/gitlab-certified-project-management-specialist-bundle). Make sure that you use the employee discount code in the link to enroll for free.
- [ ] Review [how to use GitLab planning features](https://about.gitlab.com/handbook/marketing/strategic-marketing/getting-started/101/) to collaborate and manage work

## Section 6 - Review Opportunity Pipeline
_Weeks 4-8_

- [ ] Login to Salesforce.com and review Pipeline:
    - [ ] Begin to review Regional Opportunities in Salesforce.com. This ['Open Opps' report](https://gitlab.my.salesforce.com/00O4M000004aGyC) provides an overview of All Open Professional Services Opportunities. Filter to show 'Current FQ' to start.
    - [ ] Begin to 'Follow' Opportunities where appplicable or of interest
    - [ ] As you begin to 'lead' Opportunities, review [this handbook page](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/engagement-mgmt/tracking-opps/), and begin to update Salesforce.com accordingly.   
- [ ] Begin scheduling shadowing opportunities to prepare for the next section

## Section 7 - Final Assessment

_Weeks 5-10_

- [ ] Review the final [EM Onboarding assignment](https://docs.google.com/document/d/1FdQpQOe_OkjvA3svAsjGfsaiDoUAFs_h0LPVYzIkvdI/edit) and associated [grading rubric](https://docs.google.com/spreadsheets/d/1HLzVSgoSJ6zDltusq8KnkpHoyDxByumko7N90Rr5MQk/edit). You will be expected to create a propsal and SOW to a panel of your peers who will act as the mock customer and/or account team. This will validate your understanding of our services, processes and overall ability to articulate our value proposition. This exercise is supposed to simulate a real customer scoping opportunity, so the process of delivering this will happen over a number of synchronous and asynchronous meetings:
    - [ ] scoping issue created - gather info asynchronously
    - [ ] Check out the [grading rubric](https://docs.google.com/spreadsheets/d/1HLzVSgoSJ6zDltusq8KnkpHoyDxByumko7N90Rr5MQk/edit?usp=sharing) we will use to evaluate your proposal.
    - [ ] initial proposal call based on current understanding
    - [ ] revised proposal call based on updated understanding (note this could happen multiple times until we can agreement in principal on the services proposal)
    - [ ] SOW review - asynch unless meeting needed


## Section 8 - Find an opportunity to lead an opportunity

_Week 6-12_

- [ ] Lead the scoping of an engagement with an account team with peer engagement manager is observing and providing feedback. 
- [ ] Use feedback from previous EM to improve on offering and gain approval from previous EM to scope deals solo. 

</details>
