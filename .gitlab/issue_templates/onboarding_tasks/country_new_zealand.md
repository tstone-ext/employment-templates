### Day 1 - For Team Members in New Zealand only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Please complete and sign the [IR330 form](https://www.ird.govt.nz/-/media/project/ir/home/documents/forms-and-guides/ir300---ir399/ir330/ir330-2019.pdf). 
2. [ ] New team member: Upload the completed form in Workday you can do so by navigating to your `Workday Profile` via your image in the upper right hand corner of the screen and selecting `Personal` followed by the `Documents Tab`. Be sure to notify People Connect that you have done so in the comments section of this issuetagging`sszepietowska` or `nprecilla` (Non US Payroll). 
3. [ ] New team member: Please review the [GitLab PTY LTD Remote Work Checklist](https://forms.gle/tT7gEDPFyWpkVfkS8). _Please complete this checklist within the first 30 days of your start date_. 
4. [ ] Request to join the [NZ Google Group](https://groups.google.com/a/gitlab.com/g/new.zealand/), this will give you access to NZ events and documents 
</details>

<details>
<summary>Legal</summary>

1. [ ] @katiecorso Review responses to remote work form and follow up with team member, if needed.
</details>

<details>
<summary>People Connect</summary>

1. [ ] People Connect: Following the guidelines documented in the [job aid](https://docs.google.com/document/d/1fKRau2JJcuHLxsHep4Z6zi6ylTSQxeQ-VsIhPKPITFc/edit) update the team members Probation Period details in Workday.
1. [ ] People Connect: Verify that the new team member's Legal Name on their Photo ID matches the Legal Name entered into Workday.

</details>
