## FOR SALES ONLY

<summary>Sales/Marketing Ops</summary>

1. [ ] Sales/Marketing Ops: Disable User and Other Actions
    * [ ] @kbetances: Update Sales Operations Sponsored Reports/Dashboards listed on [The Offboarding Sales Ops Owned Systems Handbook Page](https://internal-handbook.gitlab.io/handbook/sales/sales-operations/offboarding-sales-ops-owned-systems/#dashboards)
    * [ ] @kbetances: To offboard Accounts and Opportunities, create an issue in the [Sales Operations project](https://gitlab.com/gitlab-com/sales-team/field-operations/sales-operations/-/issues/new?issue%5Bmilestone_id%5D=#) and use the Territory Change Request template.  Select offboarding as the reason and assign to the sales manager and the related Sales Operations team member (if unknown ask in the #sops-team Slack channel).
    * [ ] @kbetances: If offboarding Sales ASM/RD/VP, replace the manager field in SFDC with the updated manager.
    * [ ] @emathis: Salesforce Leads and contacts: REASSIGN all leads and contacts based on direction provided by SDR leadership within 24 hours of termination.
    * [ ] @emathis: Update Territory Model in LeanData with temporary territory assignments for SDRs.
    * [ ] Outreach - @gillmurphy: First, lock the user, Second, turn off both "Enable sending messages from this mailbox" and "Enable syncing messages from this mailbox".
    * [ ] ZoomInfo - @robrosu: Deactivate and remove former team member from ZoomInfo
    * [ ] Cognism - @robrosu: Deactivate and remove former team member from Cognism (Applicable for BDRs/SDRs)
    * [ ] LeanData - @emathis: Remove from any lead routing rules and round robin groups.
    * [ ] LinkedIn Sales Navigator - @emathis Disable user, remove from team license.
1. [ ] Terminus: @gillmurphy Remove team member.
1. [ ] IMPartner: @ecepulis @KJaeger Remove team member from IMPartner if applicable.
1. [ ] Xactly Incent: @lisapuzar @Swethakashyap @pravi1 @sophiehamann
1. [ ] O'Reilly: @jfullam @tbopape Remove team member from O'Reilly if applicable.

<summary>TAM Sales</summary>

1. [ ] Retain.ai: @chloe @spatching Remove team member from Retain.ai if applicable.
