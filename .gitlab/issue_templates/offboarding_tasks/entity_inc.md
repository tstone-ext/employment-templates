## For GitLab Inc team members only
Payroll @vlaughlan, @ybasha

1. [ ] Update team member status to ```Terminated``` in ADP.
1. [ ] Inactivate deductions under pay profile in ADP.
