### FOR UX RESEARCHERS & PRODUCT DESIGNERS

<summary>Manager</summary>

1. [ ] Manager: Remove former team member from [Mural](https://www.mural.co/)
1. [ ] Manager: Remove former team member from UX team Google calendar and any other team calendars
1. [ ] Manager: Create an [access removal request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issue) to remove the former team member from Loom
1. [ ] Manager: Review [design pairs](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/sites/handbook/source/handbook/engineering/ux/how-we-work/design-pair-rotation/index.html.md) and make adjustments as needed
1. [ ] @laurenevans: Remove former team member from UserTesting.com
1. [ ] @cfaughnan: Remove former team member from Respondent

<summary>IT Ops</summary>

1. [ ] IT Ops @gitlab-com/business-technology/team-member-enablement : remove team member from the `@uxers` User Group on Slack.

<summary>Other</summary>

1. [ ] @laurenevans: Remove former team member from UserTesting.com
1. [ ] @cfaughnan: Remove former team member from Respondent
1. [ ] @tauriedavis: Remove former team member from [GitLab UI roulette helper](https://gitlab.com/gitlab-org/gitlab-ui/-/blob/main/danger/simple_ux_review/Dangerfile)
1. [ ] @tauriedavis or @jeldergl: Remove former team member from Figma GitLab Organization
