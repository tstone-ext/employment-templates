## FOR ENGINEERING ONLY (Devs, PEs, SEs, UX)

<summary>Manager</summary>

1. [ ] Manager: Remove former GitLab Team Member's' GitHub.com account from the [gitlabhq organization](https://github.com/orgs/gitlabhq/people) (if applicable).
1. [ ] For former Developers (those who had access to part of the infrastructure), and Production GitLab Team Members: copy offboarding process from [infrastructure](https://dev.gitlab.org/cookbooks/chef-repo/blob/master/doc/offboarding.md) for offboarding action.
1. [ ] Manager: Remove access to PagerDuty if applicable.
1. [ ] Manager (For Build Engineers): Remove team member as a member to the GitLab Dev Digital Ocean account https://cloud.digitalocean.com/settings/team.
1. [ ] Manager: Remove from GitLab Docker Hub teams if applicable.
1. [ ] Manager: Replace mentions in handbook's [stages.yml](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/stages.yml) as this is the look-up reference for many processes. 
1. [ ] Manager: Remove former team member from the Async Team Retrospective [team.yml](https://gitlab.com/gitlab-org/async-retrospectives/-/blob/master/teams.yml) file, add yourself or temporary backup instead.
1. [ ] Manager: Remove former team member from team Slack bots such as daily stand-up, if applicable.
1. [ ] Manager (For Ops Section): Remove former team member from the [ops-section-retro.md](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/issue_templates/ops-section-retro.md) issue template.
1. [ ] Manager (For R&D team): Remove former team member from the [product-development-retro.md](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/issue_templates/product-development-retro.md) issue template.
1. [ ] Manager: Remove former team member from the [_categories-names.erb](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/sites/handbook/source/includes/product/_categories-names.erb) file.
1. [ ] Manager: (For Anti-abuse team members) Remove from [Arkose Labs](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/tech_stack.yml) team if applicable. 

<summary>Product Licenses</summary>

- [ ] JetBrains (@leipert, @samdbeckham): Go to the [user management](https://account.jetbrains.com/login) and search for the team member, revoke their licenses.


## Infrastructure

#### <summary>@gitlab-com/it-infra</summary>

<table>
  <tbody>
		</tr>
		<tr>
			<td> ops.gitlab.net  </td>
			<td>

- [ ] Access Deprovisioned - ops.gitlab.net
- [ ] Not Applicable to Team Member - ops.gitlab.net

</td>
</tbody>
</table>

- [ ] @adamhuss: Remove the team member from Nira
