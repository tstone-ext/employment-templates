## For GitLab GmbH employees only

<summary>People Connect</summary>

1. [ ] People Connect: Inform payroll (@sszepietowska  @nprecilla)  of last day of employment.
1. [ ] People Connect: Insure a resignation letter with a wet signature has been saved in the Workday Termination folder. 
